﻿namespace ClientGame
{
    partial class Game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.b22 = new System.Windows.Forms.Button();
            this.b12 = new System.Windows.Forms.Button();
            this.b02 = new System.Windows.Forms.Button();
            this.b21 = new System.Windows.Forms.Button();
            this.b11 = new System.Windows.Forms.Button();
            this.b01 = new System.Windows.Forms.Button();
            this.b20 = new System.Windows.Forms.Button();
            this.b10 = new System.Windows.Forms.Button();
            this.b00 = new System.Windows.Forms.Button();
            this.vs = new System.Windows.Forms.Label();
            this.player1 = new System.Windows.Forms.Label();
            this.player2 = new System.Windows.Forms.Label();
            this.won_label = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.b22);
            this.panel1.Controls.Add(this.b12);
            this.panel1.Controls.Add(this.b02);
            this.panel1.Controls.Add(this.b21);
            this.panel1.Controls.Add(this.b11);
            this.panel1.Controls.Add(this.b01);
            this.panel1.Controls.Add(this.b20);
            this.panel1.Controls.Add(this.b10);
            this.panel1.Controls.Add(this.b00);
            this.panel1.Location = new System.Drawing.Point(67, 53);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(250, 250);
            this.panel1.TabIndex = 0;
            // 
            // b22
            // 
            this.b22.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b22.Location = new System.Drawing.Point(170, 170);
            this.b22.Name = "b22";
            this.b22.Size = new System.Drawing.Size(80, 80);
            this.b22.TabIndex = 8;
            this.b22.UseVisualStyleBackColor = true;
            this.b22.Click += new System.EventHandler(this.Square_Click);
            // 
            // b12
            // 
            this.b12.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b12.Location = new System.Drawing.Point(86, 171);
            this.b12.Name = "b12";
            this.b12.Size = new System.Drawing.Size(80, 80);
            this.b12.TabIndex = 7;
            this.b12.UseVisualStyleBackColor = true;
            this.b12.Click += new System.EventHandler(this.Square_Click);
            // 
            // b02
            // 
            this.b02.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b02.Location = new System.Drawing.Point(0, 170);
            this.b02.Name = "b02";
            this.b02.Size = new System.Drawing.Size(80, 80);
            this.b02.TabIndex = 6;
            this.b02.UseVisualStyleBackColor = true;
            this.b02.Click += new System.EventHandler(this.Square_Click);
            // 
            // b21
            // 
            this.b21.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b21.Location = new System.Drawing.Point(170, 85);
            this.b21.Name = "b21";
            this.b21.Size = new System.Drawing.Size(80, 80);
            this.b21.TabIndex = 5;
            this.b21.UseVisualStyleBackColor = true;
            this.b21.Click += new System.EventHandler(this.Square_Click);
            // 
            // b11
            // 
            this.b11.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b11.Location = new System.Drawing.Point(85, 85);
            this.b11.Name = "b11";
            this.b11.Size = new System.Drawing.Size(80, 80);
            this.b11.TabIndex = 4;
            this.b11.UseVisualStyleBackColor = true;
            this.b11.Click += new System.EventHandler(this.Square_Click);
            // 
            // b01
            // 
            this.b01.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b01.Location = new System.Drawing.Point(0, 86);
            this.b01.Name = "b01";
            this.b01.Size = new System.Drawing.Size(80, 80);
            this.b01.TabIndex = 3;
            this.b01.UseVisualStyleBackColor = true;
            this.b01.Click += new System.EventHandler(this.Square_Click);
            // 
            // b20
            // 
            this.b20.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b20.Location = new System.Drawing.Point(170, 0);
            this.b20.Name = "b20";
            this.b20.Size = new System.Drawing.Size(80, 80);
            this.b20.TabIndex = 2;
            this.b20.UseVisualStyleBackColor = true;
            this.b20.Click += new System.EventHandler(this.Square_Click);
            // 
            // b10
            // 
            this.b10.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b10.Location = new System.Drawing.Point(86, 0);
            this.b10.Name = "b10";
            this.b10.Size = new System.Drawing.Size(80, 80);
            this.b10.TabIndex = 1;
            this.b10.UseVisualStyleBackColor = true;
            this.b10.Click += new System.EventHandler(this.Square_Click);
            // 
            // b00
            // 
            this.b00.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b00.Location = new System.Drawing.Point(0, 0);
            this.b00.Name = "b00";
            this.b00.Size = new System.Drawing.Size(80, 80);
            this.b00.TabIndex = 0;
            this.b00.UseVisualStyleBackColor = true;
            this.b00.Click += new System.EventHandler(this.Square_Click);
            // 
            // vs
            // 
            this.vs.AutoSize = true;
            this.vs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vs.Location = new System.Drawing.Point(177, 19);
            this.vs.Name = "vs";
            this.vs.Size = new System.Drawing.Size(31, 20);
            this.vs.TabIndex = 1;
            this.vs.Text = "VS";
            // 
            // player1
            // 
            this.player1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.player1.Location = new System.Drawing.Point(21, 19);
            this.player1.Name = "player1";
            this.player1.Size = new System.Drawing.Size(150, 20);
            this.player1.TabIndex = 2;
            this.player1.Text = "Player 1";
            this.player1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // player2
            // 
            this.player2.AutoSize = true;
            this.player2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.player2.Location = new System.Drawing.Point(214, 19);
            this.player2.Name = "player2";
            this.player2.Size = new System.Drawing.Size(65, 20);
            this.player2.TabIndex = 3;
            this.player2.Text = "Player 2";
            // 
            // won_label
            // 
            this.won_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.won_label.Location = new System.Drawing.Point(67, 319);
            this.won_label.Name = "won_label";
            this.won_label.Size = new System.Drawing.Size(250, 23);
            this.won_label.TabIndex = 4;
            this.won_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 362);
            this.Controls.Add(this.won_label);
            this.Controls.Add(this.player2);
            this.Controls.Add(this.player1);
            this.Controls.Add(this.vs);
            this.Controls.Add(this.panel1);
            this.Name = "Game";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Game";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button b22;
        private System.Windows.Forms.Button b12;
        private System.Windows.Forms.Button b02;
        private System.Windows.Forms.Button b21;
        private System.Windows.Forms.Button b11;
        private System.Windows.Forms.Button b01;
        private System.Windows.Forms.Button b20;
        private System.Windows.Forms.Button b10;
        private System.Windows.Forms.Button b00;
        private System.Windows.Forms.Label vs;
        private System.Windows.Forms.Label player1;
        private System.Windows.Forms.Label player2;
        private System.Windows.Forms.Label won_label;
    }
}