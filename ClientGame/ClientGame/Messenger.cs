﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using MultiplayerDLL;

namespace ClientGame
{
    public class Messenger
    {
        NetworkStream stream;
        BinaryFormatter bf;

        //Dictionary<MessageType, Action<int, object>> actions;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="stream"></param>
        public Messenger(NetworkStream stream)
        {
            this.stream = stream;
            bf = new BinaryFormatter();
        }

        /// <summary>
        /// Sends message using current stream.
        /// </summary>
        /// <param name="msg"></param>
        public void SendMessage(IMessage msg)
        {
            bf.Serialize(stream, msg);
        }

        /// <summary>
        /// Checks for message in current stream.
        /// </summary>
        /// <returns></returns>
        public IMessage GetMessage()
        {
            IMessage message = (IMessage)bf.Deserialize(stream);
            return message;
        }

        //public void AddAction(MessageType type, Action<int, object> action)
        //{
        //    actions.Add(type, action);
        //}

        //public void RemoveAction(MessageType type)
        //{
        //    actions.Remove(type);
        //}

        //public void MessageLoop()
        //{
        //    while (true)
        //    {
        //        IMessage message = GetMessage();

        //        Action<int, object> method = null;
        //        actions.TryGetValue(message.Type, out method);

        //        if (method != null)
        //        {
        //            method.BeginInvoke(message.From, message.Data, method.EndInvoke, null);
        //        }
        //    }
        //}
    }
}
