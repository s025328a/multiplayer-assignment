﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using MultiplayerDLL;

namespace ClientGame
{
    public partial class Lobby : Form
    {
        TcpClient tcp_client;
        NetworkStream stream;

        BinaryFormatter bf;

        Player player;
        List<Player> all_players;

        Messenger messenger;

        Thread message_thread;

        Game game;

        bool closing;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="username"></param>
        public Lobby(TcpClient client, string username)
        {
            this.tcp_client = client;
            this.player = new Player(username);

            stream = tcp_client.GetStream();

            bf = new BinaryFormatter();
            all_players = new List<Player>();
            messenger = new Messenger(stream);
            
            InitializeComponent();

            ColumnHeader header = new ColumnHeader();
            header.Text = "";
            header.Name = "dummy";
            header.Width = 230;
            message_listview.Columns.Add(header);

            message_listview.Scrollable = true;
            message_listview.HeaderStyle = ColumnHeaderStyle.None;

            this.FormClosed += ClosedEvent;
            message_textbox.KeyPress += send_button_KeyPress;

            messenger.SendMessage(new IMessage(this.player, -1, -1, 1, MessageType.ConnectRequest));

            message_thread = new Thread(new ThreadStart(MessageHandler));
            message_thread.Start();
        }

        /// <summary>
        /// Adds a list item when using another Thread.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="item"></param>
        void AddListItem(ListView list, ListViewItem item)
        {
            if (list.InvokeRequired)
                list.Invoke(new MethodInvoker(delegate
                {
                    list.Items.Add(item);

                }));
            else
                list.Items.Add(item);
        }

        #region Messaging

        /// <summary>
        /// Run on a new thread to constantly check for messages.
        /// </summary>
        void MessageHandler()
        {
            try
            {
                while (true)
                {
                    IMessage message = messenger.GetMessage();

                    switch(message.Type)
                    {

                        case MessageType.ConnectConfirm:

                            this.player = (Player)message.Data;
                            break;

                        case MessageType.Move:

                            if (player.In_Game)
                            {
                                GameMove move = (GameMove)message.Data;
                                BeginInvoke(new Action<GameMove>(game.RecieveMove), new object[] { move });
                            }
                            break;

                        case MessageType.Text:

                            TextMessage(message.From, (string)message.Data);
                            break;

                        case MessageType.AllPlayerUpdate:

                            Player[] p_list = (Player[])message.Data;
                            this.all_players.Clear();
                            this.all_players = new List<Player>(p_list);

                            RefreshPlayerList();
                            break;

                        case MessageType.GameRequest:

                            GameRequest(message.From);
                            break;

                        case MessageType.StartGame:

                            StartGame(message.From, (bool)message.Data);
                            break;
                    }
                }
            }
            catch (Exception)
            {
                if (!closing)
                    MessageBox.Show("Disconnected from Server.", "Error");
            }
            finally
            {
                stream.Close();
            }
        }
        
        /// <summary>
        /// Handles game request message. 
        /// If user accepts, game window is launched.
        /// </summary>
        /// <param name="from"></param>
        void GameRequest(int from)
        {
            Player sender = all_players.FirstOrDefault(p => p.Id == from);
            if (sender != null)
            {
                DialogResult result = MessageBox.Show(sender.Username + " wants to play! Do you accept?", "Game Request", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    messenger.SendMessage(new IMessage(true, sender.Id, this.player.Id, 1, MessageType.StartGame));
                    player.In_Game = true;
                    game = new Game(this, this.player, sender, true, this.messenger);
                    BeginInvoke(
                        new MethodInvoker(delegate
                        {
                            game.Show();
                        })
                    );
                    UpdatePlayer();
                }
                else
                {
                    messenger.SendMessage(new IMessage(false, sender.Id, this.player.Id, 1, MessageType.StartGame));
                }
            }
        }

        /// <summary>
        /// Adds text message to message window.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="text"></param>
        void TextMessage(int from, string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                ListViewItem lvi = new ListViewItem(text);

                Player sender = all_players.FirstOrDefault(p => p.Id == from);

                if (sender != null)
                {
                    lvi.ForeColor = sender.Colour;
                }
                else
                    lvi.ForeColor = this.player.Colour;

                AddListItem(message_listview, lvi);
            }
        }

        /// <summary>
        /// Handles response message from game request. If other player accepts,
        /// a game window is launched.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="start"></param>
        void StartGame(int from, bool start)
        {            
            Player from_player = all_players.FirstOrDefault(p => p.Id == from);
            if (from_player != null)
            {
                if (start)
                {
                    player.In_Game = true;
                    game = new Game(this, this.player, from_player, false, this.messenger);
                    BeginInvoke(
                        new MethodInvoker(delegate
                        {
                            game.Show();
                        })
                    );
                    UpdatePlayer();
                }
                else
                {
                    MessageBox.Show(from_player.Username + " declined your game invite.", "Info");
                }
            }
        }

        /// <summary>
        /// Refreshes player listview from player list.
        /// </summary>
        void RefreshPlayerList()
        {
            if (player_listview.InvokeRequired)
                player_listview.Invoke(new MethodInvoker(delegate
                {
                    player_listview.Items.Clear();

                }));
            else
                player_listview.Items.Clear();
            
            foreach(Player p in all_players)
            {
                ListViewItem lvi = new ListViewItem(p.Username);
                lvi.ForeColor = p.Colour;
                lvi.Tag = p;

                AddListItem(player_listview, lvi);
            }
        }

        // Sends message to update player state on the server.
        public void UpdatePlayer()
        {
            messenger.SendMessage(new IMessage(this.player, 0, this.player.Id, 1, MessageType.PlayerUpdate));
        }

#endregion

        #region Events

        void ClosedEvent(object sender, FormClosedEventArgs e)
        {
            this.FormClosed -= ClosedEvent;
            Application.Exit();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            closing = true;
            stream.Dispose();
            tcp_client.Close();
        
            base.OnClosing(e);
        }

        private void send_button_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(message_textbox.Text))
                {
                    string text = player.Username + " (" + DateTime.Now.ToShortTimeString() + "): " + message_textbox.Text;

                    messenger.SendMessage(new IMessage(text, -1, this.player.Id, 1, MessageType.Text));

                    message_textbox.Text = string.Empty;
                }
            }
            catch
            {
                MessageBox.Show("Could not send message", "Error");
            }
        }

        void send_button_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                send_button_Click(sender, e);
                e.Handled = true;
            }
        }

        private void game_request_button_Click(object sender, EventArgs e)
        {
            if (player_listview.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please select a player!", "Warning");
                return;
            }

            Player selected_player = (Player)player_listview.SelectedItems[0].Tag;

            messenger.SendMessage(new IMessage(null, selected_player.Id, this.player.Id, 0, MessageType.GameRequest));
        }

        private void player_listview_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (player_listview.SelectedItems.Count > 0)
            {
                Player selected_player = (Player)player_listview.SelectedItems[0].Tag;
                if (selected_player.In_Game || selected_player.Id == this.player.Id)
                {
                    game_request_button.Enabled = false;
                    return;
                }
                else
                {
                    game_request_button.Enabled = true;
                }
            }
            else
            {
                game_request_button.Enabled = false;
            }
        }

        #endregion
    }
}
