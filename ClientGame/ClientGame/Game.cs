﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MultiplayerDLL;

namespace ClientGame
{
    public partial class Game : Form
    {
        Button[,] squares;

        bool my_move;

        Player player;
        Player opponent;

        string player_sign;
        string opponent_sign;

        Messenger messenger;
        Lobby lobby;
        
        bool? won = null;

        /// <summary>
        /// Constructor.
        /// Initializes 2D array of buttons and other player data.
        /// </summary>
        /// <param name="lobby"></param>
        /// <param name="player"></param>
        /// <param name="opponent"></param>
        /// <param name="my_move"></param>
        /// <param name="messenger"></param>
        public Game(Lobby lobby, Player player, Player opponent, bool my_move, Messenger messenger)
        {
            InitializeComponent();

            squares = new Button[3,3];

            // Add buttons to list
            squares[0, 0] = b00;
            squares[1, 0] = b10;
            squares[2, 0] = b20;
            squares[0, 1] = b01;
            squares[1, 1] = b11;
            squares[2, 1] = b21;
            squares[0, 2] = b02;
            squares[1, 2] = b12;
            squares[2, 2] = b22;            

            this.lobby = lobby;
            this.messenger = messenger;

            this.player = player;
            this.opponent = opponent;

            this.my_move = my_move;

            if (!my_move)
            {
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        squares[i, j].Enabled = false;
                    }
                }
            }

            this.player_sign = my_move ? "O" : "X";
            this.opponent_sign = my_move ? "X" : "O";

            player1.Text = player.Username;
            player1.ForeColor = player.Colour;

            player2.Text = opponent.Username;
            player2.ForeColor = opponent.Colour;
        }

        /// <summary>
        /// Processes player move and updates game state.
        /// Sends message to opposing player with move information.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        void MakeMove(int x, int y)
        {
            squares[x, y].Text = player_sign;

            GameMove move = new GameMove(x, y);
            messenger.SendMessage(new IMessage(move, opponent.Id, player.Id, 1, MessageType.Move));
            my_move = false;

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    squares[i, j].Enabled = false;
                }
            }
            UpdateGameState();
        }
        
        /// <summary>
        /// Processes move recieved from opponent and updates game state.
        /// </summary>
        /// <param name="next_move"></param>
        public void RecieveMove(GameMove next_move)
        {
            if (next_move != null)
            {
                squares[next_move.GetX(), next_move.GetY()].Text = opponent_sign;

                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        if (string.IsNullOrEmpty(squares[i, j].Text))
                            squares[i, j].Enabled = true;
                    }
                }
                UpdateGameState();
            }
        }

        /// <summary>
        /// Checks to see if either player has won.
        /// Sets the end game message and disables buttons.
        /// </summary>
        void UpdateGameState()
        {
            won = CheckBoard();

            if (won == true)
            {
                won_label.Text = "You win!";
                won_label.ForeColor = Color.Green;
            }
            else if (won == false)
            {
                won_label.Text = "You lose!";
                won_label.ForeColor = Color.Red;
            }
            else
            {
                bool draw = true;
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        if (string.IsNullOrEmpty(squares[i, j].Text))
                        {
                            draw = false;
                        }
                    }
                }

                if (draw)
                {
                    won_label.Text = "It's a draw!";
                    won_label.ForeColor = Color.Orange;
                }
            }

            if (won.HasValue)
            {
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        squares[i, j].Enabled = false;
                    }
                }
            }
        }

        /// <summary>
        /// Checks through each combination of winning options
        /// to see if either player has won.
        /// </summary>
        /// <returns></returns>
        bool? CheckBoard()
        {
            string s;
            for (int i = 0; i < 3; i++)
            {
                s = squares[i, 0].Text;
                if (squares[i, 1].Text == s &&
                    squares[i, 2].Text == s &&
                    !string.IsNullOrEmpty(s))
                {
                    return s == player_sign ? true : false;
                }
            }

            for (int j = 0; j < 3; j++)
            {
                s = squares[0, j].Text;
                if (squares[1, j].Text == s &&
                    squares[2, j].Text == s &&
                    !string.IsNullOrEmpty(s))
                {
                    return s == player_sign ? true : false;
                }
            }

            s = squares[0, 0].Text;
            if (squares[1, 1].Text == s &&
                squares[2, 2].Text == s &&
                !string.IsNullOrEmpty(s))
            {
                return s == player_sign ? true : false;
            }

            s = squares[2, 0].Text;
            if (squares[1, 1].Text == s &&
                squares[0, 2].Text == s &&
                !string.IsNullOrEmpty(s))
            {
                return s == player_sign ? true : false;
            }

            return null;
        }

        /// <summary>
        /// Event handler for button click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Square_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;

            int y = int.Parse(b.Name.Last().ToString());
            int x = int.Parse(b.Name[b.Name.Length - 2].ToString());

            MakeMove(x, y);
        }

        /// <summary>
        /// On closing event. Sets player state to out of game,
        /// and updates player list in the game lobby.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(CancelEventArgs e)
        {
            player.In_Game = false;
            lobby.UpdatePlayer();

            OnDispose();
            base.OnClosing(e);
        }

        /// <summary>
        /// Dispose method.
        /// </summary>
        void OnDispose()
        {
            squares = null;

            my_move = false;
            
            player_sign = null;
            opponent_sign = null;
            
            won = null;
        }
    }
}
