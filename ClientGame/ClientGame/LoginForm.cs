﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace ClientGame
{
    public partial class LoginForm : Form
    {
        TcpClient tcp_client;

        /// <summary>
        /// Constructor
        /// </summary>
        public LoginForm()
        {
            InitializeComponent();
            tcp_client = new TcpClient();
        }

        /// <summary>
        /// Event handler for login button. Includes validation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void connect_button_Click(object sender, EventArgs e)
        {
            IPAddress serverIP;
            int port;

            if (!IPAddress.TryParse(serverIP_textbox.Text, out serverIP))
            {
                MessageBox.Show("Invalid IP address.", "Warning");
                return;
            }

            if (!Int32.TryParse(port_textbox.Text, out port))
            {
                MessageBox.Show("Invalid port number.", "Warning");
                return;
            } 

            if (string.IsNullOrEmpty(username_textbox.Text))
            {
                MessageBox.Show("Please enter a username.", "Warning");
                return;
            }

            try 
            {
                tcp_client.Connect(serverIP, port);
            }
            catch
            {
                MessageBox.Show("Cannot connect to server.", "Error");
                return;
            }

            Lobby lobby = new Lobby(tcp_client, username_textbox.Text);
            lobby.Show();
            this.Hide();
        }
        
        /// <summary>
        /// If user presses enter, the login button event is triggered.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void LoginForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                connect_button_Click(sender, e);
                e.Handled = true;
            }
        }
    }
}
