﻿namespace ClientGame
{
    partial class Lobby
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.player_listview = new System.Windows.Forms.ListView();
            this.label2 = new System.Windows.Forms.Label();
            this.message_textbox = new System.Windows.Forms.TextBox();
            this.send_button = new System.Windows.Forms.Button();
            this.message_listview = new System.Windows.Forms.ListView();
            this.game_request_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(137, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(370, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome to the Game Lobby!";
            // 
            // player_listview
            // 
            this.player_listview.Location = new System.Drawing.Point(337, 84);
            this.player_listview.MultiSelect = false;
            this.player_listview.Name = "player_listview";
            this.player_listview.Size = new System.Drawing.Size(249, 245);
            this.player_listview.TabIndex = 1;
            this.player_listview.UseCompatibleStateImageBehavior = false;
            this.player_listview.View = System.Windows.Forms.View.List;
            this.player_listview.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.player_listview_ItemSelectionChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(334, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Current players:";
            // 
            // message_textbox
            // 
            this.message_textbox.Location = new System.Drawing.Point(58, 84);
            this.message_textbox.Multiline = true;
            this.message_textbox.Name = "message_textbox";
            this.message_textbox.Size = new System.Drawing.Size(240, 39);
            this.message_textbox.TabIndex = 3;
            // 
            // send_button
            // 
            this.send_button.Location = new System.Drawing.Point(58, 130);
            this.send_button.Name = "send_button";
            this.send_button.Size = new System.Drawing.Size(99, 23);
            this.send_button.TabIndex = 5;
            this.send_button.Text = "Send Message";
            this.send_button.UseVisualStyleBackColor = true;
            this.send_button.Click += new System.EventHandler(this.send_button_Click);
            // 
            // message_listview
            // 
            this.message_listview.Location = new System.Drawing.Point(58, 159);
            this.message_listview.Name = "message_listview";
            this.message_listview.Size = new System.Drawing.Size(240, 199);
            this.message_listview.TabIndex = 6;
            this.message_listview.UseCompatibleStateImageBehavior = false;
            this.message_listview.View = System.Windows.Forms.View.Details;
            // 
            // game_request_button
            // 
            this.game_request_button.Enabled = false;
            this.game_request_button.Location = new System.Drawing.Point(337, 335);
            this.game_request_button.Name = "game_request_button";
            this.game_request_button.Size = new System.Drawing.Size(249, 23);
            this.game_request_button.TabIndex = 7;
            this.game_request_button.Text = "Send Game Request";
            this.game_request_button.UseVisualStyleBackColor = true;
            this.game_request_button.Click += new System.EventHandler(this.game_request_button_Click);
            // 
            // Lobby
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 442);
            this.Controls.Add(this.game_request_button);
            this.Controls.Add(this.message_listview);
            this.Controls.Add(this.send_button);
            this.Controls.Add(this.message_textbox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.player_listview);
            this.Controls.Add(this.label1);
            this.Name = "Lobby";
            this.Text = "Lobby";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView player_listview;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox message_textbox;
        private System.Windows.Forms.Button send_button;
        private System.Windows.Forms.ListView message_listview;
        private System.Windows.Forms.Button game_request_button;
    }
}