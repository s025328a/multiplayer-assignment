﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Windows;
using System.Threading.Tasks;
using System.Threading;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Concurrent;
using System.Drawing;
using MultiplayerDLL;

namespace GameServer
{
    class MessageQueue
    {
        ConcurrentDictionary<int, NetworkStream> client_streams;
        ConcurrentQueue<IMessage> message_queue;
        BinaryFormatter global_formatter;

        int current_message_id = 0;

        Dictionary<MessageType, Action<int, object>> actions;

        /// <summary>
        /// Constructor. 
        /// Initializes variables.
        /// </summary>
        public MessageQueue()
        {
            client_streams = new ConcurrentDictionary<int, NetworkStream>();
            message_queue = new ConcurrentQueue<IMessage>();
            global_formatter = new BinaryFormatter();
            actions = new Dictionary<MessageType, Action<int, object>>();
        }

        /// <summary>
        /// Sends message to all current streams.
        /// </summary>
        /// <param name="message"></param>
        void SendMessageToAll(IMessage message)
        {
            foreach (KeyValuePair<int, NetworkStream> s in client_streams)
            {
                global_formatter.Serialize(s.Value, message);
            }
        }

        /// <summary>
        /// Adds message to back of queue.
        /// </summary>
        /// <param name="msg"></param>
        public void AddMessage(IMessage msg)
        {
            msg.Id = current_message_id;
            message_queue.Enqueue(msg);
            current_message_id++;
        }

        /// <summary>
        /// Adds a client stream to the list.
        /// </summary>
        /// <param name="player_id"></param>
        /// <param name="stream"></param>
        public void AddStream(int player_id, NetworkStream stream)
        {
            client_streams.GetOrAdd(player_id, stream);
        }

        /// <summary>
        /// Removes stream from list, when client disconnects.
        /// </summary>
        /// <param name="player_id"></param>
        public void RemoveStream(int player_id)
        {
            NetworkStream s;
            client_streams.TryRemove(player_id, out s);
        }
        
        /// <summary>
        /// Adds an action method to be triggered for a specific message type.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="action"></param>
        public void AddAction(MessageType type, Action<int, object> action)
        {
            actions.Add(type, action);
        }

        /// <summary>
        /// Message loop which is run on a new Thread.
        /// </summary>
        public void MessageLoop()
        {
            List<int> priority_ids = new List<int>();
            while (true)
            {
                while (message_queue.Count > 0)
                {
                    try
                    {
                        IMessage message = null;
                        message = message_queue.FirstOrDefault(m => m.Priority == 1 && !priority_ids.Contains(m.Id));

                        if (message != null)
                        {
                            priority_ids.Add(message.Id);
                        }
                        else
                        {
                            bool priority = true;
                            while (priority)
                            {
                                message_queue.TryDequeue(out message);
                                if (priority_ids.Contains(message.Id))
                                {
                                    if (message_queue.Count == 0)
                                    {
                                        break;
                                    }
                                    priority_ids.Remove(message.Id);
                                }
                                else
                                {
                                    priority = false;
                                }
                            }
                            if (priority)
                                message = null;
                        }

                        if (message != null)
                        {
                            if (message.To == 0)
                            {
                                Action<int, object> method = null;
                                actions.TryGetValue(message.Type, out method);
                                
                                if (method != null)
                                {
                                    method.Invoke(message.From, message.Data);
                                }
                            }
                            else if (message.To == -1)
                            {
                                SendMessageToAll(message);
                            }
                            else
                            {
                                NetworkStream s = client_streams.FirstOrDefault(st => st.Key == message.To).Value;
                                if (s != null)
                                    global_formatter.Serialize(s, message);
                            }
                        }
                    }
                    catch
                    {
                        Console.WriteLine("Could not send message.");
                    }
                }
            }
        }
    }
}
