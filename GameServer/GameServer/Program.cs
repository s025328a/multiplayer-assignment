﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace GameServer
{
    class Program
    {
        static void Main(string[] args)
        {
            int port = 0;
            Console.WriteLine("Enter a port number:");

            while (port == 0)
            {
                string input = Console.ReadLine();

                if (!int.TryParse(input, out port))
                {
                    Console.WriteLine("Invalid port number. Please try again.");
                }
            }

            Server server = new Server(port);
            server.Run();
            server.Stop();
        }
    }
}
