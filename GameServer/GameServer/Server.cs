﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Windows;
using System.Threading;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Concurrent;
using System.Drawing;
using MultiplayerDLL;

namespace GameServer
{
    class Server
    {
        TcpListener tcp_listener;

        const int max_players = 8;
        static int total_players = 0;
        int player_count = 0;

        Color[] colours;
        Stack<Color> player_colours;

        ConcurrentDictionary<int, Player> players;

        MessageQueue message_queue;

        /// <summary>
        /// Constructor.
        /// Initializes tcp listener and other data structures.
        /// </summary>
        /// <param name="port"></param>
        public Server(int port)
        {
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress localIP = host.AddressList.LastOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);

            Console.WriteLine("IP: " + localIP);
            Console.WriteLine("Port: " + port);

            tcp_listener = new TcpListener(localIP, port);

            players = new ConcurrentDictionary<int, Player>();

            colours = new Color[max_players] { Color.Chartreuse, Color.DeepPink, Color.OrangeRed, Color.Violet, Color.Orange, Color.Green, Color.Red, Color.Navy };
            player_colours = new Stack<Color>(colours);

            message_queue = new MessageQueue();

            message_queue.AddAction(MessageType.PlayerUpdate, new Action<int, object>(UpdatePlayer));
        }

        /// <summary>
        /// Starts tcp listener.
        /// Initializes message loop.
        /// Waits for players to connect and starts a new Thread.
        /// </summary>
        public void Run()
        {
            tcp_listener.Start();

            Console.WriteLine("Waiting...");

            new Thread(new ThreadStart(message_queue.MessageLoop)).Start();

            while (player_count < max_players)
            {
                Socket socket = tcp_listener.AcceptSocket();
                new Thread(new ParameterizedThreadStart(SocketThread)).Start(socket);

                player_count++;
            }
        }

        /// <summary>
        /// Stops tcp listener.
        /// </summary>
        public void Stop()
        {
            tcp_listener.Stop();
        }

        /// <summary>
        /// Run on a new thread to handle incoming messages from a client.
        /// </summary>
        /// <param name="obj"></param>
        void SocketThread(object obj)
        {
            Socket socket = obj as Socket;

            NetworkStream stream = new NetworkStream(socket);
            
            BinaryFormatter bf = new BinaryFormatter();
            
            Player current_player = null;

            try
            {
                IMessage connect_msg = (IMessage)bf.Deserialize(stream);
                current_player = (Player)connect_msg.Data;
                current_player.Colour = player_colours.Pop();

                total_players++;
                current_player.Id = total_players;
                
                message_queue.AddStream(current_player.Id, stream);
                players.GetOrAdd(current_player.Id, current_player);

                message_queue.AddMessage(new IMessage(current_player, current_player.Id, -1, 1, MessageType.ConnectConfirm));
                UpdatePlayerList();

                Console.WriteLine(current_player.Username + " connected.");

                while (socket.Connected)
                {          
                    IMessage msg = (IMessage)bf.Deserialize(stream);
                    if (msg != null)
                        message_queue.AddMessage(msg);
                }
            }
            catch (Exception)
            {
                //Console.WriteLine(e.Message);
            }
            finally
            {
                Console.WriteLine((current_player != null ? current_player.Username : "player") + " disconnected.");
                stream.Close();
                stream.Dispose();

                player_count--;

                if (current_player != null)
                {
                    player_colours.Push(current_player.Colour);

                    message_queue.RemoveStream(current_player.Id);

                    Player p;
                    players.TryRemove(current_player.Id, out p);

                    UpdatePlayerList();
                }
            }
        }

        /// <summary>
        /// Sends a message to all players with an updated list of players.
        /// </summary>
        void UpdatePlayerList()
        {
            message_queue.AddMessage(new IMessage(players.Select(d => d.Value).ToArray<Player>(), -1, 0, 0, MessageType.AllPlayerUpdate));
        }

        /// <summary>
        /// Recieves update from individual player and updates server-side list.
        /// Sends message to update player list on each client.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="data"></param>
        void UpdatePlayer(int from, object data)
        {
            Player updated_player = (Player)data;
            foreach (Player p in players.Select(d => d.Value))
            {
                if (p.Id == updated_player.Id)
                {
                    p.In_Game = updated_player.In_Game;
                    break;
                }
            }
            UpdatePlayerList();
        }
    }
}
