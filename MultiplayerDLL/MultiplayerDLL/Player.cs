﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace MultiplayerDLL
{
    /// <summary>
    /// Defines a player as visible to the server/client.
    /// </summary>
    [Serializable]
    public class Player
    {
        int id;
        string username;
        Color colour;
        bool in_game;

        public Player(string username)
        {
            this.username = username;
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        public Color Colour
        {
            get { return colour; }
            set { colour = value; }
        }

        public bool In_Game
        {
            get { return in_game; }
            set { in_game = value; }
        }
    }
}
