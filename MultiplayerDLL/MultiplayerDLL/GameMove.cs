﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiplayerDLL
{
    /// <summary>
    /// Class to encapsulate game move data.
    /// </summary>
    [Serializable]
    public class GameMove
    {
        int x;
        int y;

        public GameMove(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public int GetX() { return x; }
        public int GetY() { return y; }
    }
}
