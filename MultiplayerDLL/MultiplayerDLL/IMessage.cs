﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiplayerDLL
{
    /// <summary>
    /// Defines the type of message to be sent.
    /// Is used to define the type of data also.
    /// </summary>
    public enum MessageType
    {
        ConnectRequest,
        ConnectConfirm,
        Text,
        GameRequest,
        StartGame,
        Move,
        PlayerUpdate,
        AllPlayerUpdate
    }

    /// <summary>
    /// Contains message data such as sender Id, destination Id, data, priority and message type.
    /// Most variables can be interacted with using Properties.
    /// </summary>
    [Serializable]
    public class IMessage
    {
        int? id;
        DateTime timestamp;
        int to;
        int from;
        int priority;
        MessageType type;
        object data;

        public IMessage(object data, int to, int from, int priority, MessageType type)
        {
            this.data = data;
            this.timestamp = DateTime.Now;
            this.from = from;
            this.to = to;
            this.priority = priority;
            this.type = type;
        }

        public IMessage(IMessage msg, int to)
        {
            this.data = msg.Data;
            this.timestamp = DateTime.Now;
            this.from = msg.From;
            this.to = to;
            this.priority = 0;
            this.type = msg.Type;
        }

        public object Data
        {
            get { return data; }
            set { data = value; }
        }

        public MessageType Type
        {
            get { return type; }
        }

        public int From
        {
            get { return from; }
            set { from = value; }
        }

        public int To
        {
            get { return to; }
            set { to = value; }
        }
        
        public int Priority
        {
            get { return priority; }
        }

        public int Id
        {
            get { return id ?? -1; }
            set 
            {
                if (id == null)
                    id = value; 
            }
        }
    }
}
